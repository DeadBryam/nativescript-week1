import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";

import { NavigateRoutingModule } from "./list-routing.module";
import { ListComponent } from "./list.component";
import { ListDeleteComponent } from "./delete/list-delete.component";

@NgModule({
    imports: [NativeScriptCommonModule, NavigateRoutingModule],
    declarations: [ListComponent, ListDeleteComponent],
    schemas: [NO_ERRORS_SCHEMA]
})
export class ListModule {}