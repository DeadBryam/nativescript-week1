import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";
import { ListComponent } from "./list.component";
import { ListDeleteComponent } from "./delete/list-delete.component";


const routes: Routes = [
    { path: "", component: ListComponent },
    { path: "delete", component: ListDeleteComponent}
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class NavigateRoutingModule { }
