import { Component, OnInit } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as app from "tns-core-modules/application";
import { ListadoService } from "../../domain/listado.service";
import { RouterExtensions } from "nativescript-angular/router";

@Component({
    selector: "List-Delete",
    templateUrl: "./list-delete.component.html",
    styleUrls: ["./../list.component.scss"]
})
export class ListDeleteComponent implements OnInit {
    constructor(
        private listado: ListadoService,
        private _routerExtensions: RouterExtensions
    ) {}

    ngOnInit(): void {}

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }

    deleteItem(id:number): void {
        this.listado.delete(id);
    }

    changePage() {
        this._routerExtensions.backToPreviousPage();
    }
}
