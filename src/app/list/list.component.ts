import { Component, OnInit } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as app from "tns-core-modules/application";
import { ListadoService } from "../domain/listado.service";
import { RouterExtensions } from "nativescript-angular/router";
import { isAndroid } from "tns-core-modules/ui/page/page";

@Component({
    selector: "List",
    templateUrl: "./list.component.html",
    styleUrls: ["./list.component.scss"]
})
export class ListComponent implements OnInit {
    listHeader:String;

    constructor(
        private listado: ListadoService,
        private _routerExtensions: RouterExtensions
    ) {
        this.listHeader = "List";
    }

    ngOnInit(): void {
        if(isAndroid){
            this.listHeader = "Hello Android :)"
        }
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }

    addToList(): void {
        this.listado.add((Math.random() * 100).toFixed());
    }

    changePage() {
        this._routerExtensions.navigate(["/list/delete"], {
            transition: {
                name: "fade"
            }
        });
    }
}
