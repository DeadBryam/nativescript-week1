import { Injectable } from "@angular/core";

@Injectable()
export class ListadoService {
    private listado: Array<String> = [];

    add(item: String):void {
        this.listado.push(item);
    }

    clear():void{
        this.listado = [];
    }

    delete(index:number):void{
        this.listado.splice(index,1);
    }

    findAll(): Array<String> {
        return this.listado;
    }
}
